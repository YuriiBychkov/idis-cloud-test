import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
    isLogin: false
}

const store = new Vuex.Store({
    state,
    getters: {
        isLogin: (state) => {
            return state.isLogin
        }
    },
    actions: {
        isLogin(context, isLogin) {
            context.commit('isLogin', isLogin)
        }
    },
    mutations: {
        isLogin(state, isLogin) {
            state.isLogin = isLogin
        }
    },
})

export default store